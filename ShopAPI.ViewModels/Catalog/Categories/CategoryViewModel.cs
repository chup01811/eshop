﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAPI.ViewModels.Catalog.Categories
{
    public class CategoryViewModel
    {
        public int ID { set; get; }
        public string Name { set; get; }

        
        public string Alias { set; get; }

        
        public string Description { set; get; }


        public IFormFile Image { set; get; }
    }
}
