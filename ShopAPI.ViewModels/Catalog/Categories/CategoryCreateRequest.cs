﻿using Microsoft.AspNetCore.Http;
using ShopAPI.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAPI.ViewModels.Catalog.Categories
{
    public class CategoryCreateRequest
    {

        
        public string Name { set; get; }

        
        public string Alias { set; get; }

        
        public string Description { set; get; }

       
        public IFormFile Image { set; get; }
    }
}
