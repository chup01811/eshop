﻿using ShopAPI.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAPI.ViewModels.Catalog.Categories
{
    public class GetPublicCategoryPagingRequest : PagingRequestBase
    {
        public int? CategoryId { get; set; }
    }
}
