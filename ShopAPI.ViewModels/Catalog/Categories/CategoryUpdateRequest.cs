﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAPI.ViewModels.Catalog.Categories
{
    public class CategoryUpdateRequest
    {
        public int ID { set; get; }

        
        public string Name { set; get; }

        
        public string Alias { set; get; }

        
        public string Description { set; get; }

        
        public IFormFile Image { set; get; }
    }
}
