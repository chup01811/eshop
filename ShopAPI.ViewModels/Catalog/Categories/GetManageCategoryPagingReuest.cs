﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAPI.ViewModels.Catalog.Categories
{
    public class GetManageCategoryPagingReuest
    {
        public string Keyword { get; set; }

        public List<int> CategoryIds { get; set; }
        public int PageSize { get; set; }
    }
}
